package com.ohem4.pms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ohem4.pms.dao.AppUserDAO;
import com.ohem4.pms.entity.AppUserDetails;

@Service
public class AppUserService {

	@Autowired
	private AppUserDAO appUserDAO;

	public AppUserService() {
		System.out.println(this.getClass().getSimpleName() + " created");
	}

	public void saveUserData(AppUserDetails appUserDetails) {
		appUserDAO.saveUserData(appUserDetails);
	}

	public void editUserData(AppUserDetails appUserDetails) {
		appUserDAO.editUserData(appUserDetails);
	}

	public void deleteUserData(AppUserDetails appUserDetails) {
		appUserDAO.deleteUserData(appUserDetails);
	}

	public List<AppUserDetails> getAppdetailsByUserId(Long id) {
		return appUserDAO.getAppdetailsByUserId(id);
	}

	public AppUserDetails getAppdetailsByUserId(Long rId, Long id) {
		return appUserDAO.getAppdetailByUserId(rId, id);
	}
}
