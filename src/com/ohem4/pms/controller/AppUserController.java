package com.ohem4.pms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ohem4.pms.entity.AppUserDetails;
import com.ohem4.pms.entity.Register;
import com.ohem4.pms.service.AppUserService;

@Controller
@RequestMapping(value = "/")
public class AppUserController {

	@Autowired
	private AppUserService appUserService;
	
	public AppUserController() {
		System.out.println(this.getClass().getSimpleName()  +" created");
	}
	
	//@PostMapping("*/saveUserData")
	@RequestMapping(value = "/saveUserData")
	public ModelAndView saveUserData(AppUserDetails appUserDetails,HttpServletRequest request) {
		System.out.println(appUserDetails);
		Register register = (Register) request.getSession().getAttribute("register");
		appUserDetails.setRegister(register);
		System.out.println(appUserDetails);
		appUserService.saveUserData(appUserDetails);
		List<AppUserDetails> list = appUserService.getAppdetailsByUserId(register.getId());
		return new ModelAndView("home.jsp","list",list);
	}
	
	@RequestMapping(value = "/showUserData")
	public ModelAndView showUserData(@RequestParam Long id, AppUserDetails appUserDetails,HttpServletRequest request) {
		System.out.println(appUserDetails);
		Register register = (Register) request.getSession().getAttribute("register");
		appUserDetails.setRegister(register);
		AppUserDetails appdetailsByUserId = appUserService.getAppdetailsByUserId(register.getId(), id);
		return new ModelAndView("edituser.jsp","detail",appdetailsByUserId);
	}
	
	@RequestMapping(value = "/editUserData")
	public ModelAndView editUserData(AppUserDetails appUserDetails,HttpServletRequest request) {
		System.out.println(appUserDetails);
		Register register = (Register) request.getSession().getAttribute("register");
		appUserDetails.setRegister(register);
		System.out.println(appUserDetails);
		appUserService.editUserData(appUserDetails);
		List<AppUserDetails> list = appUserService.getAppdetailsByUserId(register.getId());
		return new ModelAndView("home.jsp","list",list);
	}
	
	@RequestMapping(value = "/deleteUserData")
	public ModelAndView deleteUserData(@RequestParam Long id, AppUserDetails appUserDetails, HttpServletRequest request) {
		System.out.println(appUserDetails);
		Register register = (Register) request.getSession().getAttribute("register");
		appUserDetails.setRegister(register);
		System.out.println(appUserDetails);
		appUserService.deleteUserData(appUserDetails);
		List<AppUserDetails> list = appUserService.getAppdetailsByUserId(register.getId());
		return new ModelAndView("home.jsp","list",list);
	}
}
