<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="editUserData" method="post">
		<input type="text" name="id" hidden="true" value="${detail.id}">
		<div>
			<label for="appName">Application Name</label>
			<input type="text" name="appName" value="${detail.appName}">
		</div>
		<div>
			<label for="userName">User Name</label>
			<input type="text" name="userName" value="${detail.userName}">
		</div>
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" value="${detail.email}">
		</div>
		<div>
			<label for="password">Password</label>
			<input type="password" name="password" value="${detail.password}">
		</div>

		<div>
			<input type="submit" value="save">
		</div>
	</form>
</body>
</html>